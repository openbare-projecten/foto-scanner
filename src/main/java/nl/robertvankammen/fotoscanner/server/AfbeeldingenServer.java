package nl.robertvankammen.fotoscanner.server;

import nl.robertvankammen.fotoscanner.model.Afbeelding;
import nl.robertvankammen.fotoscanner.utils.Utils;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class AfbeeldingenServer {

    private final static String AFBEELDING_ENDPOINT = "/afbeeldingen";
    private final String afbeeldingUrl;
    private final HttpHeaders httpHeaders;
    private final RestTemplate restTemplate = new RestTemplate();


    public AfbeeldingenServer(Environment environment, Utils utils) {
        this.httpHeaders = utils.maakAuthHeader();
        this.afbeeldingUrl = environment.getRequiredProperty("foto-api.url") +
                environment.getRequiredProperty("foto-api.prefix") +
                AFBEELDING_ENDPOINT;
    }

    public List<Afbeelding> ophalenAfbeeldingenVanServer() {
        final ResponseEntity<Afbeelding[]> response = restTemplate.exchange(afbeeldingUrl, HttpMethod.GET, new HttpEntity<>(httpHeaders), Afbeelding[].class);

        return Optional.ofNullable(response.getBody())
                .map(afbeeldings -> new ArrayList<>(Arrays.asList(afbeeldings)))
                .orElseGet(ArrayList::new);
    }

    public void postNieuweAfbeelding(Afbeelding afbeelding) {
        final HttpEntity<Afbeelding> request = new HttpEntity<>(afbeelding, httpHeaders);
        restTemplate.exchange(afbeeldingUrl, HttpMethod.POST, request, Afbeelding.class);
    }

    public void updateAfbeelding(Afbeelding afbeelding) {
        final HttpEntity<Afbeelding> request = new HttpEntity<>(afbeelding, httpHeaders);
        restTemplate.exchange(afbeeldingUrl + "/" + afbeelding.getId(), HttpMethod.PUT, request, Afbeelding.class);
    }

    public void verwijderAfbeelding(Afbeelding afbeelding) {
        final HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        restTemplate.exchange(afbeeldingUrl + "/" + afbeelding.getId(), HttpMethod.DELETE, request, String.class);
    }
}
