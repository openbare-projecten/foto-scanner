package nl.robertvankammen.fotoscanner.server;

import nl.robertvankammen.fotoscanner.model.Persoon;
import nl.robertvankammen.fotoscanner.utils.Utils;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class PersoonServer {

    private final static String PERSONEN_ENDPOINT = "/personen";
    private final static String PERSONEN_BIJ_NAAM_ENDPOINT = "/bijNaam";
    private final RestTemplate restTemplate = new RestTemplate();
    private final String basisUrl;
    private final HttpHeaders httpHeaders;

    public PersoonServer(Environment environment, Utils utils) {
        this.httpHeaders = utils.maakAuthHeader();
        this.basisUrl = environment.getRequiredProperty("foto-api.url") +
                environment.getRequiredProperty("foto-api.prefix");
    }

    public Persoon getPersoonVanServer(String naam) {
        final ResponseEntity<Persoon> response = restTemplate.exchange(basisUrl +
                        PERSONEN_ENDPOINT +
                        PERSONEN_BIJ_NAAM_ENDPOINT +
                        "/" + naam,
                HttpMethod.GET, new HttpEntity<>(httpHeaders), Persoon.class);

        return response.getBody();
    }

    public Persoon nieuwPersoonOpslaan(Persoon persoon) {
        final HttpEntity<Persoon> request = new HttpEntity<>(persoon, httpHeaders);
        final ResponseEntity<Persoon> response = restTemplate.exchange(basisUrl + PERSONEN_ENDPOINT, HttpMethod.POST, request, Persoon.class);
        return response.getBody();
    }
}
