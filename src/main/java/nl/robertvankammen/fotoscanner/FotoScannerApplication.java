package nl.robertvankammen.fotoscanner;

import nl.robertvankammen.fotoscanner.file.FileSystemScanner;
import nl.robertvankammen.fotoscanner.model.Afbeelding;
import nl.robertvankammen.fotoscanner.scanner.VerschilScanner;
import nl.robertvankammen.fotoscanner.server.AfbeeldingenServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.List;

@SpringBootApplication
public class FotoScannerApplication {

    public static void main(String[] args) {
        ApplicationContext app = SpringApplication.run(FotoScannerApplication.class, args);

        final FileSystemScanner fileSystemScanner = app.getBean(FileSystemScanner.class);
        final AfbeeldingenServer afbeeldingenServer = app.getBean(AfbeeldingenServer.class);
        List<Afbeelding> geindexedAfbeeldingen = fileSystemScanner.indexAfbeeldingen();
        List<Afbeelding> serverAfbeeldingen = afbeeldingenServer.ophalenAfbeeldingenVanServer();

        final VerschilScanner verschilScanner = app.getBean(VerschilScanner.class);
        verschilScanner.scannVerschillen(geindexedAfbeeldingen, serverAfbeeldingen);

    }
}
