package nl.robertvankammen.fotoscanner.utils;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Component
public class Utils {

    public final static ArrayList<String> EXTENTION_LIST = new ArrayList<>(Arrays.asList("jpg", "png", "jpeg"));
    private final Environment environment;

    @Value("${gebruikersnaam:}")
    private String gebruikersnaam;
    @Value("${wachtwoord:}")
    private String wachtwoord;

    public Utils(Environment environment) {
        this.environment = environment;
    }

    public String maakUrnVanFile(File f) {
        final String extention = FilenameUtils.getExtension(f.getPath());
        if (EXTENTION_LIST.contains(extention.toLowerCase())) {
            // urn:extention:subpath1:subpath2:etc:filename
            final String restPath = f.getPath().replace(environment.getRequiredProperty("basisLocatie"), "").replace(".".concat(extention), "").replace(File.separator, ":");
            return "urn:".concat(extention).concat(":").concat(restPath);
        }
        throw new IllegalArgumentException(extention + " word niet ondersteund, de volgende worden alleen ondersteund" + EXTENTION_LIST.toString());
    }

    public String getPathVanThumb(String groote, String urn){
        final String basisPath = environment.getRequiredProperty("basisLocatie");
        return maakPathVanUrn(urn).replace(basisPath, basisPath.concat(File.separator).concat(String.valueOf(groote)));
    }

    public String maakPathVanUrn(String urn) {
        final String[] subString = urn.split(":");
        if (subString.length >= 3 && subString[0].equals("urn")) {
            final String extention = subString[1];
            if (!EXTENTION_LIST.contains(extention.toLowerCase())) {
                throw new IllegalArgumentException("De volgende extentie wordt niet ondersteund: " + extention);
            }
            String restPath = environment.getRequiredProperty("basisLocatie");
            for (int i = 2; i < subString.length; i++) {
                restPath = restPath.concat(File.separator).concat(subString[i]);
            }
            return restPath.concat(".").concat(extention);
        }
        throw new IllegalArgumentException(urn + " is geen geldige urn, deze moet bestaan uit het volgende urn:extention:subpath:etc:filename");
    }

    public LocalDateTime convertToLocalDateTimeViaInstant(long wijzigingDatum) throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        final Date datum = sdf.parse(sdf.format(wijzigingDatum));

        return datum.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    public HttpHeaders maakAuthHeader() {
        String gebruikersnaam = this.gebruikersnaam;
        String wachtwoord = this.wachtwoord;
        if (isBlank(gebruikersnaam)) {
            gebruikersnaam = environment.getRequiredProperty("foto-api.gebruikersnaam");
        }
        if (isBlank(wachtwoord)) {
            wachtwoord = environment.getRequiredProperty("foto-api.wachtwoord");
        }
        final String auth = gebruikersnaam + ":" + wachtwoord;
        return new HttpHeaders() {{
            final byte[] encodedBytes = Base64.getEncoder().encode(auth.getBytes());
            final String authHeader = "Basic " + new String(encodedBytes);
            set("Authorization", authHeader);
        }};
    }

}
