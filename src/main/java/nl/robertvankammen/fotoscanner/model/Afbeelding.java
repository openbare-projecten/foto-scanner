package nl.robertvankammen.fotoscanner.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Afbeelding {
    private long id;
    private String urn;
    private LocalDateTime laatstGewijzigdDatumTijd;
    private LocalDateTime orgineleDatum;
    private List<Persoon> personenLijst;

    public Afbeelding() {
        // voor jackson
    }

    public Afbeelding(String urn, LocalDateTime laatstGewijzigdDatumTijd) {
        this.urn = urn;
        this.laatstGewijzigdDatumTijd = laatstGewijzigdDatumTijd;
    }

    public String getUrn() {
        return urn;
    }

    public void setUrn(String urn) {
        this.urn = urn;
    }

    public LocalDateTime getLaatstGewijzigdDatumTijd() {
        return laatstGewijzigdDatumTijd;
    }

    public void setLaatstGewijzigdDatumTijd(LocalDateTime laatstGewijzigdDatumTijd) {
        this.laatstGewijzigdDatumTijd = laatstGewijzigdDatumTijd;
    }

    public long getId() {
        return id;
    }

    public List<Persoon> getPersonenLijst() {
        if (personenLijst == null) {
            personenLijst = new ArrayList<>();
        }
        return personenLijst;
    }

    public LocalDateTime getOrgineleDatum() {
        return orgineleDatum;
    }

    public void setOrgineleDatum(LocalDateTime orgineleDatum) {
        this.orgineleDatum = orgineleDatum;
    }
}
