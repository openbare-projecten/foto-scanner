package nl.robertvankammen.fotoscanner.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Persoon {
    private Long id;
    private String naam;

    public Persoon() {
        // voor jackson
    }

    public Persoon(String naam) {
        this.naam = naam;
    }

    public Long getId() {
        return id;
    }

    public String getNaam() {
        return naam;
    }
}