package nl.robertvankammen.fotoscanner.file;

import net.coobird.thumbnailator.Thumbnails;
import nl.robertvankammen.fotoscanner.model.Afbeelding;
import nl.robertvankammen.fotoscanner.utils.Utils;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class Verkleiner {

    private final Utils utils;
    public String[] grootes = {"256", "720"};

    public Verkleiner(Utils utils) {
        this.utils = utils;
    }

    public void maakKleinereAfbeelding(int maxPixels, Afbeelding afbeelding) {
        final String path = utils.maakPathVanUrn(afbeelding.getUrn());
        final String outPath = utils.getPathVanThumb(String.valueOf(maxPixels), afbeelding.getUrn());
        try {
            File outFile = new File(outPath);
            if (!outFile.exists()) {
                outFile.getParentFile().mkdirs();
                Thumbnails.of(new File(path)).size(maxPixels, maxPixels).toFile(outFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
