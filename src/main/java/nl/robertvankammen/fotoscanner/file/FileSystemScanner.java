package nl.robertvankammen.fotoscanner.file;

import nl.robertvankammen.fotoscanner.model.Afbeelding;
import nl.robertvankammen.fotoscanner.utils.Utils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

import static nl.robertvankammen.fotoscanner.utils.Utils.EXTENTION_LIST;

@Component
public class FileSystemScanner {

    private final ArrayList<String> uitgeslotenMappen = new ArrayList<>();
    private final Utils utils;
    private final Environment environment;
    private final ArrayList<Afbeelding> afbeeldingenLijst = new ArrayList<>();

    public FileSystemScanner(Utils utils, Environment environment, Verkleiner verkleiner) {
        this.utils = utils;
        this.environment = environment;
        uitgeslotenMappen.add("profielfotos");
        uitgeslotenMappen.addAll(Arrays.asList(verkleiner.grootes));

    }

    public ArrayList<Afbeelding> indexAfbeeldingen() {
        File basisLocation = new File(environment.getRequiredProperty("basisLocatie"));
        recursiveScan(basisLocation);
        System.out.println("klaar met indexing, " + afbeeldingenLijst.size() + " afbeeldingen gevonden");
        return afbeeldingenLijst;
    }

    private void recursiveScan(File file) {
        File[] files = file.listFiles();
        if (files != null && files.length > 0) {
            Stream.of(files).forEach(f -> {
                if (f.isDirectory()) {
                    if (uitgeslotenMappen.stream().noneMatch(s -> f.getPath().contains(s))) {
                        recursiveScan(f);
                    }
                } else {
                    try {
                        if (EXTENTION_LIST.contains(FilenameUtils.getExtension(f.getPath()).toLowerCase())) {
                            afbeeldingenLijst.add(
                                    new Afbeelding(
                                            utils.maakUrnVanFile(f),
                                            utils.convertToLocalDateTimeViaInstant(f.lastModified()))
                            );
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
