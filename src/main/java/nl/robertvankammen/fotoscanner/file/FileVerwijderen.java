package nl.robertvankammen.fotoscanner.file;

import nl.robertvankammen.fotoscanner.model.Afbeelding;
import nl.robertvankammen.fotoscanner.utils.Utils;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class FileVerwijderen {

    private final Utils utils;

    public FileVerwijderen(Utils utils){
        this.utils = utils;
    }

    public void verwijderKleinereAfbeeldingen(String groote, Afbeelding afbeelding){
        final String outPath = utils.getPathVanThumb(groote, afbeelding.getUrn());
        File file = new File(outPath);
        file.delete();
    }
}
