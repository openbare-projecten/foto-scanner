package nl.robertvankammen.fotoscanner.scanner;

import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPIterator;
import com.adobe.xmp.XMPMeta;
import com.adobe.xmp.properties.XMPPropertyInfo;
import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.xmp.XmpDirectory;
import nl.robertvankammen.fotoscanner.model.Afbeelding;
import nl.robertvankammen.fotoscanner.model.Persoon;
import nl.robertvankammen.fotoscanner.server.PersoonServer;
import nl.robertvankammen.fotoscanner.utils.Utils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static nl.robertvankammen.fotoscanner.utils.Utils.EXTENTION_LIST;

@Component
public class AfbeeldingScanner {

    private final PersoonServer persoonServer;
    private final Utils utils;

    public AfbeeldingScanner(PersoonServer persoonServer, Utils utils) {
        this.persoonServer = persoonServer;
        this.utils = utils;
    }

    public void scannAfbeelding(Afbeelding afbeelding) {
        final File file = new File(utils.maakPathVanUrn(afbeelding.getUrn()));
        if (EXTENTION_LIST.contains(FilenameUtils.getExtension(file.getPath()).toLowerCase())) {
            try {
                final Metadata metadata = ImageMetadataReader.readMetadata(file);
                setOrgineleDatum(metadata, afbeelding);
                setPersonen(metadata, afbeelding);
                afbeelding.setLaatstGewijzigdDatumTijd(utils.convertToLocalDateTimeViaInstant(file.lastModified()));
            } catch (IOException | ImageProcessingException | XMPException | ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private void setPersonen(Metadata metadata, Afbeelding afbeelding) throws XMPException {
        final XmpDirectory xmpDirectory = metadata.getFirstDirectoryOfType(XmpDirectory.class);
        if (xmpDirectory != null) {
            final XMPMeta xmpMeta = xmpDirectory.getXMPMeta();
            final XMPIterator itr = xmpMeta.iterator();
            while (itr.hasNext()) {
                final XMPPropertyInfo pi = (XMPPropertyInfo) itr.next();
                if (pi != null && pi.getPath() != null) {
                    if ((pi.getPath().endsWith("mwg-rs:Name"))) {
                        getOfMaakPersoon(pi.getValue(), afbeelding);
                    }
                }
            }
        }
    }

    private void getOfMaakPersoon(String naam, Afbeelding afbeelding) {
        Persoon persoon = persoonServer.getPersoonVanServer(naam);
        if (persoon == null) {
            persoon = persoonServer.nieuwPersoonOpslaan(new Persoon(naam));
        }
        afbeelding.getPersonenLijst().add(persoon);
    }

    private void setOrgineleDatum(Metadata metadata, Afbeelding afbeelding) {
        final ExifSubIFDDirectory exifSubIFDDirectory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
        if (exifSubIFDDirectory != null && exifSubIFDDirectory.getDateOriginal() != null) {
            afbeelding.setOrgineleDatum(LocalDateTime.ofInstant(exifSubIFDDirectory.getDateOriginal().toInstant(), ZoneId.systemDefault()));
        } else {
            // todo hier misschien nog wat mee doen??
            System.out.println(afbeelding.getUrn() + " heeft geen orginele datum??");
            afbeelding.setOrgineleDatum(LocalDateTime.of(2018,10,10,10,10));
        }
    }
}
