package nl.robertvankammen.fotoscanner.scanner;

import nl.robertvankammen.fotoscanner.file.FileVerwijderen;
import nl.robertvankammen.fotoscanner.file.Verkleiner;
import nl.robertvankammen.fotoscanner.model.Afbeelding;
import nl.robertvankammen.fotoscanner.server.AfbeeldingenServer;
import nl.robertvankammen.fotoscanner.utils.Utils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import static java.lang.Thread.sleep;

@Component
public class VerschilScanner {

    private final Verkleiner verkleiner;
    private final AfbeeldingScanner afbeeldingScanner;
    private final AfbeeldingenServer afbeeldingenServer;
    private final FileVerwijderen fileVerwijderen;
    private ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);

    public VerschilScanner(Verkleiner verkleiner, AfbeeldingenServer afbeeldingenServer, AfbeeldingScanner afbeeldingScanner, FileVerwijderen fileVerwijderen) {
        this.verkleiner = verkleiner;
        this.afbeeldingenServer = afbeeldingenServer;
        this.afbeeldingScanner = afbeeldingScanner;
        this.fileVerwijderen = fileVerwijderen;
        System.out.println(executor.getQueue().remainingCapacity());
    }

    public void scannVerschillen(List<Afbeelding> geindexAfbeeldingen, List<Afbeelding> serverAfbeeldingen) {
        final List<Afbeelding> nieuweAfbeeldingen = new ArrayList<>();
        final List<Afbeelding> bestaandeAfbeeldingen = new ArrayList<>();
        final List<Afbeelding> teUpdatenAfbeeldingen = new ArrayList<>();
        final HashMap<String, Afbeelding> serverAfbeeldingenIndexMetUrn = new HashMap<>();
        final HashMap<String, Afbeelding> geindexAfbeeldingenIndexMetUrn = new HashMap<>();
        serverAfbeeldingen.forEach(afbeelding -> serverAfbeeldingenIndexMetUrn.put(afbeelding.getUrn(), afbeelding));
        geindexAfbeeldingen.forEach(afbeelding -> geindexAfbeeldingenIndexMetUrn.put(afbeelding.getUrn(), afbeelding));

        geindexAfbeeldingen.forEach(afbeelding -> {
            if (serverAfbeeldingenIndexMetUrn.get(afbeelding.getUrn()) == null) {
                nieuweAfbeeldingen.add(afbeelding);
            } else {
                bestaandeAfbeeldingen.add(afbeelding);
            }
        });

        System.out.println(nieuweAfbeeldingen.size() + " nieuwe afbeeldingen gevonden");
        nieuweAfbeeldingen(nieuweAfbeeldingen);

        System.out.println(bestaandeAfbeeldingen.size() + " bestaande afbeeldingen gevonden");
        bestaandeAfbeeldingen.forEach(afbeelding -> {
            Afbeelding serverAfbeelding = serverAfbeeldingenIndexMetUrn.get(afbeelding.getUrn());
            if (serverAfbeelding.getLaatstGewijzigdDatumTijd().isBefore(afbeelding.getLaatstGewijzigdDatumTijd())) {
                teUpdatenAfbeeldingen.add(serverAfbeelding);
            }
        });
        System.out.println(teUpdatenAfbeeldingen.size() + " te updaten afbeeldingen gevonden");
        updateAfbeeldingen(teUpdatenAfbeeldingen);

        final List<Afbeelding> teVerwijderenAfbeeldingen = new ArrayList<>();
        serverAfbeeldingen.forEach(afbeelding -> {
            if (!geindexAfbeeldingenIndexMetUrn.containsKey(afbeelding.getUrn())) {
                teVerwijderenAfbeeldingen.add(afbeelding);
            }

        });
        System.out.println(teVerwijderenAfbeeldingen.size() + " te verwijderen afbeeldingen gevonden");
        verwijderAfbeeldingen(teVerwijderenAfbeeldingen);

        while (executor.getQueue().size() + executor.getActiveCount() > 0) {
            System.out.println(executor.getQueue().size() + executor.getActiveCount() + " nog in de que");
            try {
                sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("klaar");
    }

    private void verwijderAfbeeldingen(List<Afbeelding> verwijderenAfbeeldingen) {
        verwijderenAfbeeldingen.forEach(afbeelding -> executor.submit(() -> {
            afbeeldingenServer.verwijderAfbeelding(afbeelding);
            for (String groote : verkleiner.grootes) {
                fileVerwijderen.verwijderKleinereAfbeeldingen(groote, afbeelding);
            }
        }));

    }

    private void nieuweAfbeeldingen(List<Afbeelding> nieuweAfbeeldingenLijst) {
        nieuweAfbeeldingenLijst.forEach(afbeelding -> executor.submit(() -> {
            afbeeldingScanner.scannAfbeelding(afbeelding);
            for (String groote : verkleiner.grootes) {
                verkleiner.maakKleinereAfbeelding(Integer.valueOf(groote), afbeelding);
            }
            afbeeldingenServer.postNieuweAfbeelding(afbeelding);
        }));
    }

    private void updateAfbeeldingen(List<Afbeelding> updateAfbeeldingenLijst) {
        updateAfbeeldingenLijst.forEach(afbeelding -> executor.submit(() -> {
            afbeeldingScanner.scannAfbeelding(afbeelding);
            afbeeldingenServer.updateAfbeelding(afbeelding);
        }));
    }
}
